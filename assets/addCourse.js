let createCourse = document.querySelector('#createCourse');

createCourse.addEventListener("submit", (e) => {

	e.preventDefault();

	let courseName = document.getElementById('courseName').value;
	let coursePrice = document.getElementById('coursePrice').value;
	let courseDesc = document.getElementById('courseDesc').value;

	 if(courseName !== "" || coursePrice !== "" || courseDesc !== ""){
	 	//console.log(courseName);

	 	let token = localStorage.getItem("token");

	 	fetch("http://localhost:3000/api/courses/add-courses",{

			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`

			},
			body: JSON.stringify(
				{
					name: courseName,
					price: coursePrice,
					description: courseDesc
				}
			)// parsing the object to a JSON string

		}).then(result => result.json())
		.then(result => {
				if(result){
					alert(`Course Successfully created`);
					window.location.replace('./courses.html');
				} else {
					alert(`Course Creation Failed`);
				}
			}
		)
	 } else {
	 	alert(`Please input all fields`);
	 }


})