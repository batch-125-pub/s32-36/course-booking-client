let loginUser = document.querySelector('#loginUser');

localStorage.clear(); //to clear the local storage first when logging in for the first time

loginUser.addEventListener("submit", (e) => {
	e.preventDefault();

	let email = document.querySelector('#email').value;
	let password = document.querySelector('#password').value;

	if(email === "" || password === ""){
		alert(`Please input required fields`);
	} else {
		fetch('http://localhost:3000/api/users/login', {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}).then(result=> result.json())
		.then(result=> {
			console.log("token", result);
			localStorage.setItem("token", result.access); //stores logged user in the local storage' syntax: localStorage.setItem(<key>, <value>)


			if(result.access){
				fetch('http://localhost:3000/api/users/details', {
					method: "GET",
					headers: {
						"Authorization" : `Bearer ${result.access}`
					}
				}).then(result => result.json())
				.then(result=>{
					localStorage.setItem("id", result._id);
					localStorage.setItem("isAdmin", result.isAdmin);
					window.location.replace('./courses.html');s
				});
			}
		})
	}

})