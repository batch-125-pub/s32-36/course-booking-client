//target html elements
	//target the form
		//getElementById
		//getElementByClassName
		//getElementByTagName

	//querySelector

let registerForm = document.querySelector('#registerUser');

//add event listener
registerForm.addEventListener("submit", (e) => {

e.preventDefault();

	let firstName = document.getElementById('firstName').value;
	let lastName = document.getElementById('lastName').value;
	let mobileNo = document.getElementById('mobileNo').value;
	let email = document.getElementById('email').value;
	let password = document.getElementById('password').value;
	let password2 = document.getElementById('password2').value;

	if(password === password2 && mobileNo.length < 11 && password !== "" && password2 !== ""){
		
		//fetch -> transmits requests and responses from the server
		//fetch(<url>, {options})
		//console.log(firstName);
		fetch("http://localhost:3000/api/users/checkEmail", {

			method: "POST",
			header: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(
				{
					email: email
				}
			)// parsing the object to a JSON string

		}).then(result=>result.json())
		.then(result=>{
		//console.log(result);
		if(result === false){
			console.log("inside if statement", result);
			console.log("this is the firstName", firstName);
			fetch("http://localhost:3000/api/users/register", 
				{
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password
					})
				}
			).then(result=> result.json())
			.then(result=> {
				console.log("result in registration", result);
				if(result === true){
					alert("Registered successfully");
					window.location.replace('./login.html'); //traverse from register.html
				}else{
					alert("Not registered");
				}
			})
		} else {
			alert(`Email is already existing, enter another`);
		}
		//result is parsed back to an object (from string to object)
		//end of fetch and then for checkEmail

	}) //end of then
}


}); //end of event listener
